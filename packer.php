<?
session_start();
if (isset($_REQUEST['pwd'])){
	$_SESSION["packer_password"] = $_REQUEST['pwd'];
}
if (!isset($_SESSION["packer_password"]) || $_SESSION["packer_password"] != "ab852456") die();

if (empty($_REQUEST["module"]) || empty($_REQUEST["tpl"])){
	echo '<form method="post">';
	$dirs = preg_grep('/^([^.])/', scandir($_SERVER["DOCUMENT_ROOT"]."/app/addons/"));
	echo '<select name="module">';

		foreach ($dirs as $file) {
			echo '<option value="'.$file.'">'.$file.'</option>';
		}

	echo '</select>';
	
	$dirs = preg_grep('/^([^.])/', scandir($_SERVER["DOCUMENT_ROOT"]."/design/themes"));
	echo '<select name="tpl">';
	
		foreach ($dirs as $file) {
			echo '<option value="'.$file.'">'.$file.'</option>';
		}

	echo '</select>';
	echo '<input type="submit" value="download">';
	echo '</form>';
	die();
}


$tpl_name = $_REQUEST["tpl"];

$module = $_REQUEST["module"];
$archive = $_SERVER["DOCUMENT_ROOT"]."/$module.zip";

$folders = Array();

$folders["app"] = $_SERVER["DOCUMENT_ROOT"]."/app/addons/$module";
$folders["js"] = $_SERVER["DOCUMENT_ROOT"]."/js/addons/$module";
$folders["design_backend"] = $_SERVER["DOCUMENT_ROOT"]."/design/backend/templates/addons/$module";
$folders["design_backend_mail"] = $_SERVER["DOCUMENT_ROOT"]."/design/backend/mail/templates/addons/$module";
$folders["design_backend_css"] = $_SERVER["DOCUMENT_ROOT"]."/design/backend/css/addons/$module";
$folders["design_backend_media"] = $_SERVER["DOCUMENT_ROOT"]."/design/backend/media/images/addons/$module";

$folders["design_frontend"] = $_SERVER["DOCUMENT_ROOT"]."/design/themes/$tpl_name/templates/addons/$module";
$folders["design_frontend_mail"] = $_SERVER["DOCUMENT_ROOT"]."/design/themes/$tpl_name/mail/templates/addons/$module";
$folders["design_frontend_css"] = $_SERVER["DOCUMENT_ROOT"]."/design/themes/$tpl_name/css/addons/$module";
$folders["design_frontend_media"] = $_SERVER["DOCUMENT_ROOT"]."/design/themes/$tpl_name/media/images/addons/$module";

$folders["langs"] = $_SERVER["DOCUMENT_ROOT"]."/var/langs/{lang}/addons/";


$zip = new ZipArchive;
if ($zip->open($archive, ZipArchive::OVERWRITE) === TRUE) {
	foreach ($folders as $folder_name => $folder) {
		if ($folder_name == "langs"){
			$dirs = preg_grep('/^([^.])/', scandir($_SERVER["DOCUMENT_ROOT"]."/var/langs/"));
			foreach ($dirs as $dir_name) {
				$file = str_replace("{lang}", $dir_name, $folder)."$module.po";
				if (file_exists($file)){
					$new_file = str_replace($_SERVER["DOCUMENT_ROOT"]."/", "", $file); 
					$zip->addFromString($new_file, $file) or die ("ERROR: Could not add file: $key </br> numFile:".$zip->numFiles);
				    $zip->addFile($file, $new_file) or die ("ERROR: Could not add file: $key </br> numFile:".$zip->numFiles);
				}
			}
		}
		else
		{
			if (is_dir($folder)){
				$dirlist = new RecursiveDirectoryIterator($folder,RecursiveDirectoryIterator::SKIP_DOTS);
				$filelist = new RecursiveIteratorIterator($dirlist);
			}
			foreach ($filelist as $file) {
			    $new_file = str_replace($_SERVER["DOCUMENT_ROOT"]."/", "", $file); 

			    if (!file_exists($file)) { die($file.' does not exist. Please contact your administrator or try again later.'); }
			    if (!is_readable($file)) { die($file.' not readable. Please contact your administrator or try again later.'); }    
			    $zip->addFromString($new_file, $file) or die ("ERROR: Could not add file: $key </br> numFile:".$zip->numFiles);
			    $zip->addFile($file, $new_file) or die ("ERROR: Could not add file: $key </br> numFile:".$zip->numFiles);

			    if (preg_match("/frontend/i", $folder_name)){
			    	$new_file = str_replace("design/themes/$tpl_name/", "var/themes_repository/basic/", $new_file);
			    	$zip->addFromString($new_file, $file);
			    	$zip->addFile($file, $new_file);	
			    }
			}
		}
	}
    $zip->close();
    
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=' . basename($archive));
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($archive));
    readfile($archive);
    unlink($archive);
    exit;
}
?>